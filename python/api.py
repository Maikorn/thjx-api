#! /usr/bin/env python2
# -*- coding: utf-8 -*-

import json
import socket
import hashlib


class API(object):

    # Create the API object and connect to the server at the given address
    # and port.
    def __init__(self, address, port):
        self.socket = socket.create_connection((address, port))

    def __sendJSON(self, datas):
        self.socket.send(json.dumps(datas).encode('ascii'))

    # Send a connect message, with the password as a SHA512 hash.
    def sendConnect(self, login, password):
        token = hashlib.sha512(login + "_thjx_" + password).hexdigest().upper()
        self.__sendJSON({"type": "connect",
                         "login": login,
                         "password": token})

    # Send the next move to play by the player. This action will move the
    # source stack to the dest stack. It is not verified by the API.
    def sendMove(self, login, source, dest):
        self.__sendJSON({"type": "move",
                         "player": login,
                         "source": source,
                         "dest": dest})

    def sendAck(self):
        self.__sendJSON({"type": "ack"})

    def __recvJSON(self, expectedType):
        data = ""
        keepGoing = True
        while keepGoing:
            try:
                keepGoing = False
                data += self.socket.recv(4096)
                obj = json.loads(data)
            except ValueError:
                keepGoing = True
        if obj["type"] == expectedType:
            return obj
        return None

    # Receive the game message from the server. It only checks if the JSON
    # object has the correct type, and returns None if it is not the case.
    def recvGame(self):
        return self.__recvJSON("game")

    def recvPlay(self):
        return self.__recvJSON("play")

    # Receive the move message from the server. It only checks if the JSON
    # object has the correct type, and returns None if it is not the case
    def recvMove(self):
        return self.__recvJSON("move")

    # Receive the results message from the server. It only checks if the
    # JSON object has the correct type, and returns None if it is not the
    # case.
    def recvResults(self):
        return self.__recvJSON("results")
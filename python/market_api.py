#! /usr/bin/env python2
# -*- coding: utf-8 -*-

import json
import socket
import hashlib


class MarketAPI(object):

    # Create the API object and connect to the server at the given address
    # and port.
    def __init__(self, address, port):
        self.socket = socket.create_connection((address, port))

    def __sendJSON(self, datas):
        self.socket.send(json.dumps(datas).encode('ascii'))

    # Send a connect message, with the password as a SHA512 hash.
    def sendConnect(self, login, password):
        token = hashlib.sha512(login + "_thjx_" + password).hexdigest().upper()
        self.__sendJSON({"type": "connect",
                         "login": login,
                         "password": token})

    def sendIntroOrder(self, order_from, nb_titles):
        """
        Introducing nb_titles titles in the market. Determines the initial value of your titles.
        :param order_from: your login
        :param nb_titles: nb_titles you are introducing in the market. Between 100k and 1M.
        """
        if 100000 <= nb_titles <= 1000000:
            self.__sendJSON({"type": "intro_order",
                             "from": order_from,
                             "nb_titles": nb_titles})

    def sendOrder(self, order_from, sign, value, nb_titles, company_titles):
        """
        Send an order from yourself to buy or sell nb_titles from company_titles.
        :param order_from: your login.
        :param sign: "+" for a buy order, "-" for a sell order.
        :param value: value of transaction, in µpts. Set to -1 if it is a prioritized order.
        :param nb_titles: number of titles in this transaction,
        :param company_titles: 'label' of company originally owning those titles.
        """
        self.__sendJSON({"type": "order",
                         "from": order_from,
                         "sign": sign,
                         "value": value,
                         "nb_titles": nb_titles,
                         "company_titles": company_titles})

    def cancelOrder(self, order_id):
        self.__sendJSON({"type": "cancel_order",
                         "order_id": order_id})

    def getOrders(self, login=""):
        self.__sendJSON({"type": "get_orders",
                         "titles": login})

    def getCash(self):
        self.__sendJSON({"type": "get_cash"})

    def getPhase(self):
        self.__sendJSON({"type": "get_phase"})

    def __recvJSON(self, expectedType):
        data = ""
        keepGoing = True
        while keepGoing:
            try:
                keepGoing = False
                data += self.socket.recv(4096)
                obj = json.loads(data)
            except ValueError:
                keepGoing = True
        if obj["type"] == expectedType:
            return obj
        return None

    def recvOrderAck(self):
        return self.__recvJSON("order_ack")

    def recvCancelOrderAck(self):
        return self.__recvJSON("cancel_order_ack")

    def recvCash(self):
        return self.__recvJSON("cash")

    def recvOrders(self):
        return self.__recvJSON("orders")

    def recvPhase(self):
        return self.__recvJSON("phase")

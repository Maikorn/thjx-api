#! /usr/bin/env python2
# -*- coding: utf-8 -*-

import api
import sys

script, host, port = sys.argv

api = api.API(host, port)
login = "login"
api.sendConnect(login, "password")

game = api.recvGame()
K = game["bricks_per_color"]
p = game["nb_colors"]
# TODO: Create board with K and p (as an array, a dict or whatever...)

while True:
    if game["first_player"] == game["you"]:
        # my move
        api.recvPlay()
        api.sendMove(login, 0, 1)  # found by your IA
        # TODO: update the board with my_move
        res = api.recvResults()
        api.sendAck()
        if res["ended"]:
            print "Winner is", res["winner"]
            break
        # other's move
        other_move = api.recvMove()
        api.sendAck()
        # TODO: update the board with other_move
        res = api.recvResults()
        api.sendAck()
        if res["ended"]:
            print "Winner is", res["winner"]
            break
    else:
        # other's move
        other_move = api.recvMove()
        api.sendAck()
        # TODO: update the board with other_move
        res = api.recvResults()
        api.sendAck()
        if res["ended"]:
            print "Winner is", res["winner"]
            break
        # my move
        api.recvPlay()
        api.sendMove(login, 0, 1)  # found by your IA
        # TODO: update the board with my_move
        res = api.recvResults()
        api.sendAck()
        if res["ended"]:
            print "Winner is", res["winner"]
            break

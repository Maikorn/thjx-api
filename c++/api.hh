#ifndef API_HH_

# define API_HH_

# include <boost/asio.hpp>
# include <map>
# include <string>
# include "json.hh"

class API
{
    public:
        /*
         * Create the API object and connect to the server at the given address
         * and port.
         */
        API(const std::string& address, const std::string& port);


        /*
         * Send a connect message, with the password as a SHA512 hash.
         */
        void sendConnect(const std::string& login, const std::string& password);

        /*
         * Send the next move to play by the player. This action will move the
         * source stack to the dest stack. It is not verified by the API.
         */
        void sendMove(const std::string& login,
                      int source,
                      int dest);


        void sendAck();

        /*
         * Receive the game message from the server. It only checks if the JSON
         * object has the correct type, and returns an empty object if it is
         * not the case.
         */
        Json::Value recvGame();

        void recvPlay();

        /*
         * Receive the move message from the server. It only checks if the JSON
         * object has the correct type, and returns an empty object if it is
         * not the case.
         */
        Json::Value recvMove();

        /*
         * Receive the results message from the server. It only checks if the
         * JSON object has the correct type, and returns an empty object if it
         * is not the case.
         */
        Json::Value recvResults();

    private:
        Json::Value _recvJSON();
        void _sendJSON(const Json::Value& data);

        boost::asio::io_service _IOService;
        boost::asio::ip::tcp::socket _socket;
        boost::asio::ip::tcp::resolver _resolver;
};

#endif /* !API_HH_ */

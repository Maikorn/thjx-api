#ifdef __APPLE__
#include <cryptopp/hex.h>
#include <cryptopp/sha.h>
#else
#include <crypto++/hex.h>
#include <crypto++/sha.h>
#endif
#include <istream>
#include "market_api.hh"

MarketAPI::MarketAPI(const std::string& address, const std::string& port)
    : _IOService(), _socket(_IOService), _resolver(_IOService)
{
    boost::asio::connect(_socket,
                         _resolver.resolve(boost::asio::ip::tcp::resolver::query(address, port)));
}

Json::Value MarketAPI::_recvJSON()
{
    boost::asio::streambuf buffer;
    bool parsingSuccessful = false;
    Json::Value data;

    while (!parsingSuccessful)
    {
        std::size_t size = _socket.read_some(buffer.prepare(4096));
        buffer.commit(size);
        std::istream is(&buffer);

        Json::Reader reader;
        parsingSuccessful = reader.parse(is, data);
    }

    return data;
}

void MarketAPI::_sendJSON(const Json::Value& data)
{
    Json::StyledWriter writer;
    boost::asio::write(_socket, boost::asio::buffer(writer.write(data)));
}


void MarketAPI::sendConnect(const std::string& login, const std::string& password)
{
    Json::Value data;
    data["type"] = "connect";
    data["login"] = login;

    CryptoPP::SHA512 hash;
    byte digest[CryptoPP::SHA512::DIGESTSIZE];

    std::string token = login + "_thjx_" + password;
    hash.CalculateDigest(digest, (byte*)token.c_str(), token.length());

    CryptoPP::HexEncoder encoder;
    token.clear();
    encoder.Attach(new CryptoPP::StringSink(token));
    encoder.Put(digest, sizeof(digest));
    encoder.MessageEnd();
    data["password"] = token;

    _sendJSON(data);
}

void MarketAPI::sendIntroOrder(const std::string& order_from, int nb_titles)
{
    Json::Value data;
    data["type"] = "intro_order";
    data["from"] = order_from;
    data["nb_titles"] = nb_titles;
    if (100000 <= nb_titles && nb_titles <= 1000000)
        _sendJSON(data);
}

void MarketAPI::sendOrder(const std::string& order_from,
                          const std::string& sign,
                          int value,
                          int nb_titles,
                          const std::string& company_titles)
{
    Json::Value data;
    data["type"] = "order";
    data["from"] = order_from;
    data["sign"] = sign;
    data["value"] = value;
    data["nb_titles"] = nb_titles;
    data["company_titles"] = company_titles;
    _sendJSON(data);
}

void MarketAPI::cancelOrder(int order_id)
{
    Json::Value data;
    data["type"] = "cancel_order";
    data["order_id"] = order_id;
    _sendJSON(data);
}

void MarketAPI::getOrders(const std::string& login)
{
    Json::Value data;
    data["type"] = "get_orders";
    data["titles"] = login;
    _sendJSON(data);
}

void MarketAPI::getCash()
{
    Json::Value data;
    data["type"] = "get_cash";
    _sendJSON(data);
}

void MarketAPI::getPhase()
{
    Json::Value data;
    data["type"] = "get_phase";
    _sendJSON(data);
}

Json::Value MarketAPI::recvOrderAck()
{
    Json::Value data = _recvJSON();
    if (data.get("type", "").asString() != "order_ack")
        return Json::Value();
    return data;
}

Json::Value MarketAPI::recvCancelOrderAck()
{
    Json::Value data = _recvJSON();
    if (data.get("type", "").asString() != "cancel_order_ack")
        return Json::Value();
    return data;
}

Json::Value MarketAPI::recvCash()
{
    Json::Value data = _recvJSON();
    if (data.get("type", "").asString() != "cash")
        return Json::Value();
    return data;
}

Json::Value MarketAPI::recvOrders()
{
    Json::Value data = _recvJSON();
    if (data.get("type", "").asString() != "orders")
        return Json::Value();
    return data;
}

Json::Value MarketAPI::recvPhase()
{
    Json::Value data = _recvJSON();
    if (data.get("type", "").asString() != "phase")
        return Json::Value();
    return data;
}
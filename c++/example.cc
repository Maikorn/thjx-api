#include <map>
#include "api.hh"
using std::cout;
using std::endl;

int main(int argc, char** argv)
{
    if (argc != 3)
        return 1;

    API api(argv[1], argv[2]);
    std::string login = "login";
    api.sendConnect(login, "password");

    Json::Value game = api.recvGame();
    // TODO: get the board and create yours
    cout << "recvGame : " << game << endl;

    Json::Value res;
    Json::Value other_move;
    while (true)
    {
        if (game.get("first_player", "").asString() == game.get("you", "").asString())
        {
            // my move
            api.recvPlay();
            api.sendMove(login, 0, 1); // found by your IA
            // TODO: update the board with my_move
            res = api.recvResults();
            api.sendAck();
            if (res.get("ended", false).asBool())
                break;
            // other's move
            other_move = api.recvMove();
            api.sendAck();
            // TODO: update the board with other_move
            res = api.recvResults();
            api.sendAck();
            if (res.get("ended", false).asBool())
                break;
        }
        else
        {
            // other's move
            other_move = api.recvMove();
            api.sendAck();
            // TODO: update the board with other_move
            res = api.recvResults();
            api.sendAck();
            if (res.get("ended", false).asBool())
                break;
            // my move
            api.recvPlay();
            api.sendMove(login, 0, 1); // found by your IA
            // TODO: update the board with my_move
            res = api.recvResults();
            api.sendAck();
            if (res.get("ended", false).asBool())
                break;
        }
    }
    return 0;
}

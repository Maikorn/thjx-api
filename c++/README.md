What is it?
===========

A simple C++ API for the THJX project. It is functional and easy to use, but it
is very limited (for example, there is almost *NO* data validation).


How to use it?
==============

Juste run `make all` to obtain the 'libapi.a'.
You can compile the example with `make test`. This example ('example.cc') is
intended to show you how to use the API.

The API uses several libraries:

*   [jsoncpp](http://sourceforge.net/projects/jsoncpp/), a nice JSON parser in
    C++. It is already in the tarball so you don't have to install anything;
*   [Boost Asio](http://www.boost.org/doc/libs/1_55_0/doc/html/boost_asio.html),
    a part of the famous Boost library dedicated to network and low-level I/O
    programming;
*   [Crypto++](http://www.cryptopp.com/), a good, multi-platform, cryptographic
    library in C++ (used for the SHA512).

The documentation is the comments in 'api.hh'. It is the only header you have
to read. For more information on the message exchange, please read the full
specifications.

Disclaimer
==========

This API is only an example, to help you start your project. You do not have to
use it, you can reimplement it if you wish.

There is *no* warranty of the validity of this API. It has been tested, but
some bugs may remain, they will probably not be fixed.

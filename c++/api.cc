#ifdef __APPLE__
#include <cryptopp/hex.h>
#include <cryptopp/sha.h>
#else
#include <crypto++/hex.h>
#include <crypto++/sha.h>
#endif
#include <istream>
#include "api.hh"

API::API(const std::string& address, const std::string& port)
    : _IOService(), _socket(_IOService), _resolver(_IOService)

{
    boost::asio::connect(_socket,
                         _resolver.resolve(boost::asio::ip::tcp::resolver::query(address, port)));
}

Json::Value API::_recvJSON()
{
    boost::asio::streambuf buffer;
    bool parsingSuccessful = false;
    Json::Value data;

    while (!parsingSuccessful)
    {
        std::size_t size = _socket.read_some(buffer.prepare(4096));
        buffer.commit(size);
        std::istream is(&buffer);

        Json::Reader reader;
        parsingSuccessful = reader.parse(is, data);
    }

    return data;
}

void API::_sendJSON(const Json::Value& data)
{
    Json::StyledWriter writer;
    boost::asio::write(_socket, boost::asio::buffer(writer.write(data)));
}


void API::sendConnect(const std::string& login, const std::string& password)
{
    Json::Value data;
    data["type"] = "connect";
    data["login"] = login;

    CryptoPP::SHA512 hash;
    byte digest[CryptoPP::SHA512::DIGESTSIZE];

    std::string token = login + "_thjx_" + password;
    hash.CalculateDigest(digest, (byte*)token.c_str(), token.length());

    CryptoPP::HexEncoder encoder;
    token.clear();
    encoder.Attach(new CryptoPP::StringSink(token));
    encoder.Put(digest, sizeof(digest));
    encoder.MessageEnd();
    data["password"] = token;

    _sendJSON(data);
}

void API::sendMove(const std::string& login,
                   int source,
                   int dest)
{
    Json::Value data;
    data["type"] = "move";
    data["player"] = login;
    data["source"] = source;
    data["dest"] = dest;
    _sendJSON(data);
}

void API::sendAck()
{
    Json::Value data;
    data["type"] = "ack";
    _sendJSON(data);
}

Json::Value API::recvGame()
{
    Json::Value data = _recvJSON();
    if (data.get("type", "").asString() != "game")
        return Json::Value();
    return data;
}

void API::recvPlay()
{
    assert(_recvJSON().get("type", "").asString() == "play");
}

Json::Value API::recvMove()
{
    Json::Value data = _recvJSON();
    if (data.get("type", "").asString() != "move")
        return Json::Value();
    return data;
}

Json::Value API::recvResults()
{
    Json::Value data = _recvJSON();
    if (data.get("type", "").asString() != "results")
        return Json::Value();
    return data;
}

#ifndef MARKET_API_HH_

# define MARKET_API_HH_

# include <boost/asio.hpp>
# include <map>
# include <string>
# include "json.hh"

class MarketAPI
{
    public:
        /*
         * Create the MarketAPI object and connect to the server at the given address
         * and port.
         */
        MarketAPI(const std::string& address, const std::string& port);


        /*
         * Send a connect message, with the password as a SHA512 hash.
         */
        void sendConnect(const std::string& login, const std::string& password);

        void sendIntroOrder(const std::string& order_from, int nb_titles);

        void sendOrder(const std::string& order_from,
                       const std::string& sign,
                       int value,
                       int nb_titles,
                       const std::string& company_titles);

        void cancelOrder(int order_id);

        void getOrders(const std::string& login);

        void getCash();

        void getPhase();

        Json::Value recvOrderAck();

        Json::Value recvCancelOrderAck();

        Json::Value recvCash();

        Json::Value recvOrders();

        Json::Value recvPhase();

    private:
        Json::Value _recvJSON();
        void _sendJSON(const Json::Value& data);

        boost::asio::io_service _IOService;
        boost::asio::ip::tcp::socket _socket;
        boost::asio::ip::tcp::resolver _resolver;
};

#endif /* !MARKET_API_HH_ */

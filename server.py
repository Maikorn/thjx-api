#! /usr/bin/env python2.7
# -*- coding: utf-8 -*-

import json
import socket
import sys
import random


def sendJSON(conn, datas):
    print "Sending: ", datas
    conn.send(json.dumps(datas).encode('ascii'))


def recvJSON(conn, expectedType):
    data = ""
    keepGoing = True
    while keepGoing:
        try:
            keepGoing = False
            data += conn.recv(4096)
            obj = json.loads(data)
        except ValueError:
            keepGoing = True
    print "Received: ", obj
    if obj["type"] == expectedType:
        return obj
    return None


def top_color(stack):
    return stack.split(" ")[-1]


def exec_move(board, source, dest):
    if source == -1 or dest == -1 or source == dest \
            or board[source] == "" or board[dest] == "":
        return False
    if len(board[source].split(' ')) == len(board[dest].split(' ')) \
            or top_color(board[source]) == top_color(board[dest]):
        board[dest] += " " + board[source]
        board[source] = ""
        return True
    return False


# Socket stuff
s = socket.socket()
print "socket", s, "ok"
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
print "socket options ok"
s.bind((sys.argv[1], int(sys.argv[2])))
print "socket bound to", sys.argv[1], "on port", sys.argv[2]
s.listen(2)
print "socket listening maximum 2 connexions"
s1 = s.accept()[0]
print s1
s2 = s.accept()[0]
print s2

# Accepting the connection
pl1 = recvJSON(s1, "connect")["login"]
print pl1
pl2 = recvJSON(s2, "connect")["login"]
print pl2

# Setting up the game
K = 3  # truly, it should be random.randint(2**32 - 1)
p = 4  # truly, it should be random.randint(2**8 - 1)

## Board is now an array :
board = []
for i in range(p):
    for j in range(K):
        board.append(str(i))

## With an array, it would be : exec_move(board, 0, 1) => the new specs prefer this way !!


print board

first_player = random.choice((pl1, pl2))

sendJSON(s1, {"type": "game",
              "you": pl1,
              "challenger": pl2,
              "first_player": first_player,
              "bricks_per_color": K,
              "nb_colors": p})
sendJSON(s2, {"type": "game",
              "you": pl2,
              "challenger": pl1,
              "first_player": first_player,
              "bricks_per_color": K,
              "nb_colors": p})

if first_player == pl1:
    sock_p1 = s1
    sock_p2 = s2
    p1 = pl1
    p2 = pl2
else:
    sock_p1 = s2
    sock_p2 = s1
    p1 = pl2
    p2 = pl1

while True:
    # first player move
    sendJSON(sock_p1, {"type": "play"})
    m1 = recvJSON(sock_p1, "move")
    # both players know what first player played
    sendJSON(sock_p2, {"type": "move",
                       "player": m1["player"],
                       "source": m1["source"],
                       "dest": m1["dest"]})
    recvJSON(sock_p2, "ack")
    # if valid move, update board, otherwise game is lost. Both players are alerted in any case.
    if exec_move(board, m1["source"], m1["dest"]):
        print board
        results = {"type": "results", "ended": False, "winner": ""}
        sendJSON(sock_p1, results)
        recvJSON(sock_p1, "ack")
        sendJSON(sock_p2, results)
        recvJSON(sock_p2, "ack")
    else:
        endgame_results = {"type": "results", "ended": True, "winner": p2}
        print endgame_results
        sendJSON(sock_p1, endgame_results)
        recvJSON(sock_p1, "ack")
        sendJSON(sock_p2, endgame_results)
        recvJSON(sock_p2, "ack")
        break

    # second player move
    sendJSON(sock_p2, {"type": "play"})
    m2 = recvJSON(sock_p2, "move")
    # both players know what second player played
    sendJSON(sock_p1, {"type": "move",
                       "player": m2["player"],
                       "source": m2["source"],
                       "dest": m2["dest"]})
    recvJSON(sock_p1, "ack")
    # if empty move, game is lost and both players are alerted.
    if exec_move(board, m2["source"], m2["dest"]):
        print board
        results = {"type": "results", "ended": False, "winner": ""}
        sendJSON(sock_p1, results)
        recvJSON(sock_p1, "ack")
        sendJSON(sock_p2, results)
        recvJSON(sock_p2, "ack")
    else:
        endgame_results = {"type": "results", "ended": True, "winner": p1}
        print endgame_results
        sendJSON(sock_p1, endgame_results)
        recvJSON(sock_p1, "ack")
        sendJSON(sock_p2, endgame_results)
        recvJSON(sock_p2, "ack")
        break

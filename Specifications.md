Valid messages
==============

*   Connection from a player (the password to send is the SHA512 of login + "_thjx_" + password, in uppercased hexadecimal)

        {
           "type": "connect",
           "login": "login_x",
           "password": "..." // sha512(login + "_thjx_" + password).hexdigest().upper()
        }

*   New game from the server

        {
          "type": "game",
          "you": "login_x",
          "challenger": "login_y",
          "first_player": "login_x", // Déterminé au random
          "bricks_per_color": 3,     // var aléa. K
          "nb_colors": 4,            // var aléa. p
        }

*   Authorisation from the server to a player to begin computations and send move

		{
		  "type": "play" // Go ahead, compute and send me your move
		}

*   Move from a player

        {
          "type": "move",
          "player": "login_x",
          "source": p1, // -1 if no valid move is left
          "dest": p2    // -1 if no valid move is left
        }

*   Endgame from the server

        {
          "type": "results",
          "ended": true,
          "winner": "login_x"
        }

*   Acknowledgment

        {
          "type": "ack"
        }


Typical messages flow
=====================

1. Only once (at the beginning):

  * Player => CONNECT

2. At the beginning of each game:

  * Server <= GAME

3. At each game turn:

  * Server <= PLAY

  * Player1 => MOVE

  * Server <= MOVE (to Player2)

  * Player2 => ACK

  * Server <= RESULTS (possibly announcing that game is over)

  * Players => ACK (possible ending here)

  * Server <= PLAY

  * Player2 => MOVE

  * Server <= MOVE (to Player1)

  * Player1 => ACK

  * Server <= RESULTS (possibly announcing that game is over)

  * Players => ACK (possible ending here)


Market API:
===========

*	Intro order

		{
			"type": "intro_order",
			"from": "login_x",
			"nb_titles": 100000  # between 100k and 1M => determines initial value of your titles.
		}

*   Order from login_x, corresponding to the triplet O = (sign, value, nb_titles), who wants to buy/sell login_y titles.

		{
			"type": "order",
			"from": "login_x",
			"sign": "+" | "-",   # + for buy, - for sell, "" for buying at introduction value if still available
			"value": 100000,     # µpts, or -1 for an order "at any price" (or for buying at introduction value if still available)
			"nb_titles": 1000,
			"company_titles": "login_y"
		}

*	Order acknowledgement, from the server, after each order sent to the server:

		{
			"type": "order_ack",
			"valid": true | false
			"order_id": 123   # if you want to cancel it later, or -1 if the order is invalid
		}

*   Cancel order:

		{
			"type": "cancel_order",
			"order_id": 123
		}

*   Cancel order acknowledgment:

		{
			"type": "cancel_order_ack",
			"cancelled": true | false,  # if false, order was already executed and/or couldn't be cancelled.
			"order_id": 123
		}

*   Get the list of public orders from the server:

		{
			"type": "get_orders"
			"titles": "login_x"   # or "" if you want to get all orders
		}

*   List of orders, from the server:

		{
			"type": "orders",
			"titles": "login_x"  # or "" if all orders
			"buy_orders": { list of buy orders }
			"sell_orders": { list of sell orders }
		}

*   Get your cash from the server:

		{
			"type": "get_cash"
		}

*   Cash response of the server:

		{
			"type": "cash",
			"value": 5000000  # in µpts
			"virtual": { list of {"login" : nb_titles_possessed} }
		}

*   Get from the server the phase the stock market is in:

		{
			"type": "get_phase"
		}

*   Phase of the stock market, from the server:

		{
			"type": "phase",
			"name": "fixing" // or "free market", or "clout", or "intro"
		}
